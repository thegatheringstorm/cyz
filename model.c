#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <ctype.h>
#include <strings.h>
#include <string.h>
#include <sys/stat.h>
#include <pthread.h>
#include <sys/wait.h>
#include <stdlib.h>
#include "stdio.h"
#include "sqlite3.h"
#include "head.h"
static int is_head = 1;

static int refresh_callback(void *fp, int argc, char **argv, char **azColName){
	(FILE *)fp;
	int i;
	if(is_head)
		is_head=0;
	else
		fputs (",",fp) ;
	char jsondata[500]={0};
	char jsonelement[100]={0};
	jsondata[0]='{';

	for(i=0; i<argc; i++){
		sprintf(jsonelement,"\"%s\":%d",azColName[i],atoi(argv[i]));
		strcat(jsondata,jsonelement);
		memset(jsonelement, 0, sizeof(jsonelement));
		if(i!=(argc-1)) strcat(jsondata,",");
		fputs(jsondata,fp);
		memset(jsondata, 0, sizeof(jsondata));
	}
	strcat(jsondata,"}");

	fputs(jsondata,fp);
	return 0;
}

static int callback(void *NotUsed, int argc, char **argv, char **azColName){
	int i;
	for(i=0; i<argc; i++){
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	}
	printf("\n");
	return 0;
}
static int chart_callback(void *sum_each_ioo, int argc, char **argv, char **azColName){
	int i;
	/*for(i=0; i<argc; i++){
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "0");
	}*/
        i = atoi(argv[0] ? argv[0] : "0");
        *(int *)sum_each_ioo = i;
	return 0;
}
void db_insert(int date_time, int ioo, int amount){
	sqlite3 *db;
	char *zErrMsg = 0;
	int rc;
	char *sql;
	char sql_written[100];

   /* Open database */
	rc = sqlite3_open("test.db", &db);
	if( rc ){
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		exit(0);
	}else{
		fprintf(stderr, "Opened database successfully\n");
	}

   /* Create SQL statement */
	sql = "INSERT INTO RECORD (date_time,amount,ioo) VALUES (%d, %d, %d);";
	sprintf(sql_written,sql,date_time,amount,ioo);
   /* Execute SQL statement */
	rc = sqlite3_exec(db, sql_written, callback, 0, &zErrMsg);
	if( rc != SQLITE_OK ){
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}else{
		fprintf(stdout, "Records created successfully\n");
	}
	sqlite3_close(db);
	refresh_json();
}

void db_delete(int id){
	sqlite3 *db;
	char *zErrMsg = 0;
	int rc;
	char *sql;
	char sql_written[100];

   /* Open database */
	rc = sqlite3_open("test.db", &db);
	if( rc ){
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		exit(0);
	}else{
		fprintf(stderr, "Opened database successfully\n");
	}

   /* Create SQL statement */
	sql = "DELETE FROM RECORD WHERE id = %d;";
	sprintf(sql_written,sql,id);
   /* Execute SQL statement */
	rc = sqlite3_exec(db, sql_written, callback, 0, &zErrMsg);
	if( rc != SQLITE_OK ){
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}else{
		fprintf(stdout, "Records created successfully\n");
	}
	sqlite3_close(db);
	refresh_json();
}

void db_chart(int *sum_ioo){
	sqlite3 *db;
	char *zErrMsg = 0;
	int rc;
	char *sql;
	char sql_written[100];
	int sum_each_ioo;
        int * p_sum_each_ioo = &sum_each_ioo;
	int i;

	sql = "SELECT SUM(amount) from RECORD WHERE ioo=%d;";
   /* Open database */
	rc = sqlite3_open("test.db", &db);
	if( rc ){
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		exit(0);
	}else{
		fprintf(stderr, "Opened database successfully\n");
	}

   /* Create SQL statement */
	for(i=1;i<6;i++){
	    sprintf(sql_written,sql,i);
	    rc = sqlite3_exec(db, sql_written, chart_callback, (void*)(p_sum_each_ioo), &zErrMsg);
            //printf("\n%d\n",sum_each_ioo);
	    sum_ioo[i]=sum_each_ioo;
	}
   /* Execute SQL statement */
	if( rc != SQLITE_OK ){
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}else{
		fprintf(stdout, "Records created successfully\n");
	}
	sqlite3_close(db);
}

void refresh_json(void){
	sqlite3 *db;
	FILE *fp;
	char *zErrMsg = 0;
	int rc;
	char *sql;
	int i;
	const char* data = "Callback function called";

	sql = "SELECT * from RECORD ORDER BY date_time DESC";

	fp = fopen("htdocs/db.json","w+");
	fputs ("[",fp);
	rc = sqlite3_open("test.db", &db);
	if( rc ){
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		exit(0);
	}else{
		fprintf(stderr, "Opened database successfully\n");
	}

   /* Create SQL statement */


   /* Execute SQL statement */
	rc = sqlite3_exec(db, sql, refresh_callback, (void *)fp, &zErrMsg);
	if( rc != SQLITE_OK ){
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}else{
		fprintf(stdout, "Operation done successfully\n");
	}
	sqlite3_close(db);

	(FILE *) fp;
	fputs ("]\n",fp);
	fclose(fp);
	is_head=1;
}
