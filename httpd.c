#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <ctype.h>
#include <strings.h>
#include <string.h>
#include <sys/stat.h>
#include <pthread.h>
#include <sys/wait.h>
#include <stdlib.h>
#include "head.h"
#define ISspace(x) isspace((int)(x))
#define SERVER_STRING "Server: cyzhttpd/0.1.0\r\n"
#define HTTP_ERROR_404 -1
#define HTTP_ERROR_400 -2
#define HTTP_ERROR_500 -3
#define HTTP_ERROR_501 -4


void* accept_request(void *pclient)
{
	int client = *(int*)pclient;
	char buf[1024];
	int numchars;
	int content_length;
	char postdata[1024];
	char method[255];
	char url[255];
	char path[512];
	size_t i, j;
	struct stat st;
	int cgi = 0;      /* becomes true if server decides this is a CGI
			   * program */
	char *query_string = NULL;
	FP modP = (FP)malloc(sizeof(FP));
	int router_result;

	numchars = get_line(client, buf, sizeof(buf));
	i = 0; j = 0;
	while (!ISspace(buf[j]) && (i < sizeof(method) - 1))
	{
		method[i] = buf[j];
		i++; j++;
	}
	method[i] = '\0';

	if (strcasecmp(method, "GET") && strcasecmp(method, "POST"))
	{
		unimplemented(client);
		return NULL;
	}

	if (strcasecmp(method, "POST") == 0)
		cgi = 1;

	i = 0;
	while (ISspace(buf[j]) && (j < sizeof(buf)))
		j++;
	while (!ISspace(buf[j]) && (i < sizeof(url) - 1) && (j < sizeof(buf)))
	{
		url[i] = buf[j];
		i++; j++;
	}
	url[i] = '\0';

	if (strcasecmp(method, "GET") == 0)
	{
		query_string = url;
		while ((*query_string != '?') && (*query_string != '\0'))
			query_string++;
		if (*query_string == '?')
		{
			cgi = 1;
			*query_string = '\0';
			query_string++;
		}
	}

	sprintf(path, "htdocs%s", url);
	if (path[strlen(path) - 1] == '/')
		strcat(path, "index.html");
	if (stat(path, &st) != -1)
	{
		if ((st.st_mode & S_IFMT) == S_IFDIR) strcat(path, "/index.html");
		serve_file(client, path);
	}
	else
	{
		printf("haha1\n");
		router_result=router(&url[1],method,&modP);
		printf("haha2\n");
		if(router_result==HTTP_ERROR_404){
			while ((numchars > 0) && strcmp("\n", buf))  /* read & discard headers */
				numchars = get_line(client, buf, sizeof(buf));
			not_found(client);
			return NULL;
		}
		if (router_result==HTTP_ERROR_501)
		{
			unimplemented(client);
			return NULL;
		}
		printf("haha3\n");
		if (strcasecmp(method, "GET") == 0)
		{
			query_string = url;
			while ((*query_string != '?') && (*query_string != '\0'))
				query_string++;
			if (*query_string == '?')
			{
				cgi = 1;
				*query_string = '\0';
				query_string++;
			}
		}else if(strcasecmp(method, "POST") == 0)
		{
			numchars = get_line(client, buf, sizeof(buf));
			while ((numchars > 0) && strcmp("\n", buf))
			{
				buf[15] = '\0';
				if (strcasecmp(buf, "Content-Length:") == 0)
					content_length = atoi(&(buf[16]));
				numchars = get_line(client, buf, sizeof(buf));
			}
			printf("haha4\n");
			if (content_length == -1) {
				bad_request(client);
				return NULL;
			}
			for (i = 0; i < content_length; i++) {
				recv(client, &postdata[i], 1, 0);
			}
			printf("haha5\n");
			query_string = postdata;
		}
		(*modP)(client,method,query_string);
	}//

	close(client);
	return NULL;
}



void cat(int client, FILE *resource)
{
	char buf[1024];


	fgets(buf, sizeof(buf), resource);
	while (!feof(resource))
	{
		send(client, buf, strlen(buf), 0);
		fgets(buf, sizeof(buf), resource);
	}

}





void error_die(const char *sc)
{
	perror(sc);
	exit(1);
}





int get_line(int sock, char *buf, int size)
{
	int i = 0;
	char c = '\0';
	int n;

	while ((i < size - 1) && (c != '\n'))
	{
		n = recv(sock, &c, 1, 0);
		/* DEBUG printf("%02X\n", c); */
		if (n > 0)
		{
			if (c == '\r')
			{
				n = recv(sock, &c, 1, MSG_PEEK);
				/* DEBUG printf("%02X\n", c); */
				if ((n > 0) && (c == '\n'))
					recv(sock, &c, 1, 0);
				else
					c = '\n';
			}
			buf[i] = c;
			i++;
		}
		else
			c = '\n';
	}
	buf[i] = '\0';

	return(i);
}




void serve_file(int client, const char *filename)
{
	FILE *resource = NULL;
	int numchars = 1;
	char buf[1024];

	buf[0] = 'A'; buf[1] = '\0';
	while ((numchars > 0) && strcmp("\n", buf))  /* read & discard headers */
		numchars = get_line(client, buf, sizeof(buf));

	resource = fopen(filename, "r");
	if (resource == NULL) not_found(client);
	else
	{
		if(!strcmp("htdocs/db.json", filename))
		{
			headers_json(client, filename);
			cat(client, resource);
		}
		else
		{
			headers(client, filename);
			cat(client, resource);
		}

	}
	fclose(resource);
}


int startup(u_short *port)
{
	int httpd = 0;
	struct sockaddr_in name;

	httpd = socket(PF_INET, SOCK_STREAM, 0);
	if (httpd == -1)
		error_die("socket");
	memset(&name, 0, sizeof(name));
	name.sin_family = AF_INET;
	name.sin_port = htons(*port);
	name.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind(httpd, (struct sockaddr *)&name, sizeof(name)) < 0)
		error_die("bind");
	if (*port == 0)  /* if dynamically allocating a port */
	{
		socklen_t namelen = sizeof(name);
		if (getsockname(httpd, (struct sockaddr *)&name, &namelen) == -1)
			error_die("getsockname");
		*port = ntohs(name.sin_port);
	}
	if (listen(httpd, 5) < 0)
		error_die("listen");
	return(httpd);
}

int main(void)
{
	int server_sock = -1;
	u_short port = 8080;
	int client_sock = -1;
	struct sockaddr_in client_name;
	socklen_t client_name_len = sizeof(client_name);
	pthread_t newthread;

	server_sock = startup(&port);
	printf("httpd running on port %d\n", port);

	while (1)
	{
		client_sock = accept(server_sock,
				(struct sockaddr *)&client_name,
				&client_name_len);
		if (client_sock == -1)
			error_die("accept");
		/* accept_request(client_sock); */
		if (pthread_create(&newthread , NULL, accept_request, (void*)&client_sock) != 0)
			perror("pthread_create");
	}

	close(server_sock);

	return(0);
}
