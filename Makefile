all: httpd

httpd: controller.c model.c view.c router.c sqlite3.c head.h sqlite3.h
	gcc -W -Wall -o httpd httpd.c controller.c router.c model.c view.c sqlite3.c -lpthread -ldl
clean:
	rm httpd
