#include <string.h>
#include <stdlib.h>
#define HTTP_ERROR_404 -1
#define HTTP_ERROR_400 -2
#define HTTP_ERROR_500 -3
#define HTTP_ERROR_501 -4
#define MOD_AMOUNT 5

void mod_insert(int ,char *,char *);
void mod_delete(int ,char *,char *);
void mod_chart(int ,char *,char *);
typedef void(*FP)(int,const char *,const char *);

char mod_map_url[MOD_AMOUNT][20]={"insert","delete","chart",""};
char mod_map_method[MOD_AMOUNT][5][8]={{"POST",""},{"POST",""},{"GET",""},{""}};
FP mod_map_mod[10]= {&mod_insert,&mod_delete,&mod_chart};




//memset(mod_map_url,0, sizeof(mod_map_url));
//memset(mod_map_method,0, sizeof(mod_map_method));

//strcpy(mod_map_url,"insert");
//strcpy(mod_map_method,"POST");

int string_in_array(char *string,char array[MOD_AMOUNT][20]){
	int i;
	for (i = 0; strcmp("",array[i]) ; i++)
	{
		if (!strcmp(string,array[i]))
		{
			return i;
		}
	}
	return -1;
}

int router (char *url,char *method,FP *mod_function){
	int result=0;
	result = string_in_array(url,mod_map_url);
	printf("%s\n", url);
	printf("%s\n", method);
	if(result==-1)
	{
		return HTTP_ERROR_404;
	}
	else if(string_in_array(method,mod_map_method[result])==-1)
	{
		return HTTP_ERROR_501;
	}else{
		*mod_function = mod_map_mod[result];
		return 0;
	}
}

/*int main(void)
{
	FP modP = (FP)malloc(sizeof(FP));
	router ("insert","POST",&modP);
	(*modP)(1,"a","b");
	return 0;
}*/
