#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <ctype.h>
#include <strings.h>
#include <string.h>
#include <sys/stat.h>
#include <pthread.h>
#include <sys/wait.h>
#include <stdlib.h>
#include "head.h"
#define HTTP_ERROR_404 -1
#define HTTP_ERROR_400 -2
#define HTTP_ERROR_500 -3
#define HTTP_ERROR_501 -4
#define ISspace(x) isspace((int)(x))
#define SERVER_STRING "Server: cyzhttpd/0.1.0\r\n"

void mod_insert(int client,char *method,char *query_string){
	char buf[1024];
	char one[2]={'1'};
	int numchars = 1;
	int a[3];
	printf("insert zHI\n");
	sscanf(query_string,"date_time=%d&ioo=%d&amount=%d",&a[0],&a[1],&a[2]);
	db_insert(a[0], a[1], a[2]);
	headers(client, "insert");
	send(client, one,1, 0);
}

void mod_delete(int client,char *method,char *query_string){
	char buf[1024];
	char one[2]={'1'};
	int numchars = 1;
	int a[3];
	printf("Delete zHI\n");
	sscanf(query_string,"id=%d",&a[0]);
	db_delete(a[0]);
	headers(client, "delete");
	send(client, one,1, 0);
}

void mod_chart(int client,char *method,char *query_string){
        FILE *fp;
        char buf[1024];
        int sum_ioo[6];
        char *chart_json;
	chart_json = "[{\"value\":%d,\"name\":\"Income\"},{\"value\":%d,\"name\":\"Cloth\"},{\"value\":%d,\"name\":\"Eat\"},{\"value\":%d,\"name\":\"Room\"},{\"value\":%d,\"name\":\"Go\"}]\n";
	char chart_json_written[200];
	printf("Chart zHI\n");
        db_chart(sum_ioo);
        sprintf(chart_json_written,chart_json,sum_ioo[1],sum_ioo[2],sum_ioo[3],sum_ioo[4],sum_ioo[5]);
        fp = fopen("htdocs/chart.json","w+");
        fputs(chart_json_written,fp);
        fclose(fp);
        serve_file(client, "htdocs/chart.json");
}
